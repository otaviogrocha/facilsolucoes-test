<?php

$conn = new mysqli("nome_do_servidor", "usuario", "senha", "nome_do_banco");

$sql = "SELECT Tb_banco.nome AS nome_banco, Tb_convenio.verba, Tb_contrato.codigo, Tb_contrato.data_inclusao, Tb_contrato.valor, Tb_contrato.prazo 
        FROM Tb_contrato
        INNER JOIN Tb_convenio_servico ON Tb_contrato.convenio_servico = Tb_convenio_servico.codigo
        INNER JOIN Tb_convenio ON Tb_convenio_servico.convenio = Tb_convenio.convenio
        INNER JOIN Tb_banco ON Tb_convenio.banco = Tb_banco.codigo";

$resultado = $conn->query($sql);

if ($resultado->num_rows > 0) {

  echo "<table>";
  echo "<thead><tr><th>Nome do banco</th><th>Verba</th><th>Código do contrato</th><th>Data de inclusão</th><th>Valor</th><th>Prazo</th></tr></thead>";
  while($row = $resultado->fetch_assoc()) {
    echo "<tbody><tr><td>".$row["nome_banco"]."</td><td>".$row["verba"]."</td><td>".$row["codigo"]."</td><td>".$row["data_inclusao"]."</td><td>".$row["valor"]."</td><td>".$row["prazo"]."</td></tr></tbody>";
  }
  echo "</table>";

} else {
  echo "Nenhum resultado encontrado.";
}

$conn->close();
?>

