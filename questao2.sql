SELECT Tb_banco.nome AS nome_cada_banco, 
       Tb_convenio.verba AS verba,
       MIN(Tb_contrato.data_inclusao) AS data_contrato_mais_antigo,
       MAX(Tb_contrato.data_inclusao) AS data_contrato_mais_novo,
       SUM(Tb_contrato.valor) AS soma_contrato
FROM Tb_banco
INNER JOIN Tb_convenio ON Tb_banco.codigo = Tb_convenio.banco
INNER JOIN Tb_convenio_servico ON Tb_convenio.codigo = Tb_convenio_servico.convenio
INNER JOIN Tb_contrato ON Tb_convenio_servico.codigo = Tb_contrato.convenio_servico
GROUP BY Tb_banco.nome, Tb_convenio.verba;
